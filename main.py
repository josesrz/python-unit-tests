import unittest
import math
import filecmp
import time


class TestCeil(unittest.TestCase):

    def test_ceiling(self):
        self.assertEqual(math.ceil(10.6), 11)
        self.assertEqual(math.ceil(9), 9)
        self.assertIsNot(math.ceil(10.6), 11)
        self.assertIs(type(math.ceil(12.89)), int)
        with self.assertRaises(TypeError) :
            math.ceil("hola")
        with self.assertRaises(TypeError):
            math.ceil((1,2))

    def test_math_factorial(self):
        self.assertEqual(math.factorial(1),1)
        self.assertIsNot(math.factorial(2), 3)
        self.assertIs(math.factorial(2), 1 * 2)

    def test_math_pow(self):
        self.assertEqual(math.pow(2,2),float(2 * 2))
        self.assertIsNot(math.pow(3,3),9.0)
        self.assertEqual(math.sqrt(math.pow(2,2)), 2.0)
        with self.assertRaises(TypeError):
            math.pow(2,"")
        with self.assertRaises(Exception):
            math.pow("","")

    def test_file_comp(self):
        self.assertEqual(filecmp.cmp("./file.comp.text","./file.comp.text"), True)
        self.assertIsNot(filecmp.cmp("file.txt","file.comp.text"), True)
        self.assertIs(filecmp.cmp('example.txt','file.txt'),True)
        self.assertEqual(filecmp.cmp(1,2,),False)
        with self.assertRaises(Exception):
            filecmp.cmp('file.txt')

    def test_time_clock(self):
        self.assertIsNot(time.clock(), time.clock())
        self.assertEqual(type(time.clock()), type(time.clock()))
        with self.assertRaises(Exception):
            time.clock(2)
        self.assertIsNot(time.clock(), "")
        self.assertIsNot(time.clock(), type(int))
    # def test_factorial(self):

if __name__ == '__main__':
    unittest.main()
